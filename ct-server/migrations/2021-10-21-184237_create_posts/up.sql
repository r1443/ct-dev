-- Your SQL goes heree

CREATE TABLE user_info(
    user_id BIGSERIAL PRIMARY KEY,
    password TEXT NOT NULL,
    name TEXT NOT NULL,
    age INT NOT NULL,
    email TEXT UNIQUE,
    phone_no TEXT UNIQUE
);



CREATE TABLE seller_info(
    seller_id BIGSERIAL PRIMARY KEY,
    password TEXT NOT NULL,
    name TEXT NOT NULL,
    age INT NOT NULL,
    email TEXT NOT NULL UNIQUE,
    phone_no TEXT NOT NULL UNIQUE
);

CREATE TABLE product(
    product_id BIGSERIAL PRIMARY KEY,
    seller_id BIGSERIAL NOT NULL,
    name TEXT NOT NULL,
    description TEXT NOT NULL,
    price INT NOT NULL,
    available_stock INT NOT NULL,
    FOREIGN KEY(seller_id) REFERENCES seller_info(seller_id) 
);

CREATE TABLE address(
    address_id BIGSERIAL PRIMARY KEY,
    name TEXT NOT NULL,
    locality TEXT NOT NULL,
    pincode TEXT NOT NULL,
    state TEXT NOT NULL,
    country TEXT NOT NULL,
    phone_no TEXT NOT NULl,
    seller_id BIGSERIAL,
    user_id BIGSERIAL,
    FOREIGN KEY(seller_id) REFERENCES seller_info(seller_id),
    FOREIGN KEY(user_id) REFERENCES user_info(user_id) 
);

CREATE TABLE product_order(
    id BIGSERIAL PRIMARY KEY,
    seller_id BIGSERIAL,
    product_id BIGSERIAL,
    address_id BIGSERIAL,
    price INT NOT NULL,
    user_id BIGSERIAL,
    ordered_at TIMESTAMPTZ NOT NULL DEFAULT now(),
    FOREIGN KEY(user_id) REFERENCES user_info(user_id),
    FOREIGN KEY(product_id) REFERENCES product(product_id),
    FOREIGN KEY(address_id) REFERENCES address(address_id)
);
