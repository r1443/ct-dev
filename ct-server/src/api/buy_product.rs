use {
    crate::model::*,
    crate::schema::product_order,
    crate::session::UserSession,
    crate::DbConn,
    diesel::prelude::*,
    rocket::http::{ContentType, Status},
    rocket::response::{Responder, Response},
    rocket::Request,
    super::error::ApiError,
};

#[derive(Insertable, Debug, Clone)]
#[table_name = "product_order"]
struct ProductOrderInsert {
    /// Special type for insertion 
    seller_id: i64,
    product_id: i64,
    address_id: i64,
    user_id: i64,
    price: i32,
}


// I don't know why i had to implemetn this "strange"
// rocket usually implement this trait for this for every type that derive Debug
// So do not touch this unless you have idea what you are doing
impl<'r> Responder<'r, 'static> for ProductOrder {
    fn respond_to(self, _: &'r Request<'_>) -> rocket::response::Result<'static> {
        let product_order_string = format!("{:?}", self);
        Response::build()
            .status(Status::Ok)
            .header(ContentType::JSON)
            .sized_body(
                product_order_string.len(),
                std::io::Cursor::new(product_order_string),
            )
            .ok()
    }
}


#[get("/product/<id>/buy/<address_id>")]
pub async fn buy_product(
    conn: DbConn,
    session: UserSession,
    id: i64,
    address_id: i64,
) -> Result<ProductOrder, ApiError> {
    /// creates a postgres transaction and tries to buy product
    use crate::schema::product::dsl::*;
    match handle_payment(session.clone(), id) {
        Ok(_) => (),
        Err(_err) => return Err(ApiError::TransactionError),
    };
    return conn
        .run(move |conn| {
            // here we are building a postgres transaction
            conn.build_transaction().read_write().run(
                || -> Result<ProductOrder, ApiError> {
                    // getting seller_id, price and available_stock of the product
                    // this will throw an error if product_id is not found
                    let about_product = product
                        .select((seller_id, price, available_stock))
                        .filter(product_id.eq(id))
                        .load::<(i64, i32, i32)>(conn)
                        // if product is not found
                        .map_err(|_| ApiError::ProductNotFound(id))?;
                    // since we only need one product
                    let about_product = about_product[0];
                    if about_product.2 == 0 {
                        // if product is out of stock
                        return Err(ApiError::OutOfStock(about_product.0));
                    }
                    {
                        // using this block because of namsespace clash with other table field
                        use crate::schema::address::dsl::*;
                        let _load_address = address
                            .filter(address_id.eq(id))
                            .load::<Address>(conn)
                            // if address is not found
                            .map_err(|_| ApiError::AddressNotFound(id))?;
                    }
                    let order_info = ProductOrderInsert {
                        seller_id: about_product.0,
                        product_id: id,
                        address_id,
                        user_id: session.id,
                        price: about_product.1,
                    };
                    // making a product order
                    let ordered_product: ProductOrder = diesel::insert_into(product_order::table)
                        .values(&order_info)
                        .get_result::<ProductOrder>(conn)?;
                    // setting the stock of the product to one less than available_stock
                    diesel::update(product.filter(product_id.eq(id)))
                        .set(available_stock.eq(about_product.2 - 1))
                        .execute(conn)?;
                    return Ok(ordered_product);
                },
            )
        })
        .await;
}

fn handle_payment(session: UserSession, product: i64) -> Result<(), String> {
    // TODO : Someway to handle payment here  probably using some api/payment gateway
    // we might want to store every transaction into a sql table.
    Ok(())
}

/*#[cfg(test)]*/
/*mod tests{*/
/*use crate::launch;*/
/*use rocket::http::{Status, ContentType};*/
/*use rocket::local::blocking::Client;*/

/*#[test]*/
/*fn buy_some_product(){*/
/*let client = Client::tracked(launch()).unwrap();*/
/*let req = client.get("/api/product/1/buy");*/
/*let response = req.dispatch();*/
/*}*/

/*}*/
