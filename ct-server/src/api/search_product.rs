
use{
    rocket::serde::json::Json,
    crate::model::Product,
};


// all functions for searching products




#[get("/product/search?<prompt>&<pageno>")]
pub async fn search_product(prompt : String, pageno : usize) 
    -> Json<Vec<Product>>
{
    // TODO : implement a function that calls the database for a full text 
    // search and return all the Product infos.
    // this will be used to display search product
    // we are going to use tsvector for searching
    unimplemented!();
}
