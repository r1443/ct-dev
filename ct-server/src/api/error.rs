use {
    rocket::response::{Response, Responder},
    rocket::http::{ContentType , Status},
    rocket::Request,
};


#[derive(Debug)]
pub enum ApiError{
    DatabaseError,
    ProductNotFound(i64),
    AddressNotFound(i64),
    TransactionError,
    OutOfStock(i64),
}


impl<'r> Responder<'r, 'static> for ApiError {
    fn respond_to(self, _: &'r Request<'_>) -> rocket::response::Result<'static> {
        let error_string = format!("{:?}", self);
        let status = match self {
            Self::DatabaseError => Status::new(403),
            Self::ProductNotFound(_) => Status::new(404),
            Self::TransactionError => Status::new(404),
            Self::AddressNotFound(_) => Status::new(404),
            Self::OutOfStock(_) => Status::new(404),
        };
        Response::build()
            .status(status)
            .header(ContentType::Plain)
            .sized_body(error_string.len(), std::io::Cursor::new(error_string))
            .ok()
    }
}

type DieselError = diesel::result::Error;

impl From<DieselError> for ApiError {
    fn from(_error: DieselError) -> Self {
        // TODO : implement this type to match all type of diesel Error
        return Self::DatabaseError;
    }
}
