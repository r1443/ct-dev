mod add_product;
mod buy_product;
mod search_product;
mod get_product;
mod error;

pub fn get_routes() -> Vec<rocket::Route> {
    routes![
        buy_product::buy_product,
        add_product::add_product,
        add_product::add_photos,
        search_product::search_product,
        get_product::get_product,
    ]
}
