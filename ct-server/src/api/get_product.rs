use {
    super::error::ApiError,
    crate::model::Product, 
    crate::DbConn, 
    diesel::prelude::*,
    rocket::serde::json::Json,
};



#[get("/product/<id>")]
pub async fn get_product(id: i64, conn: DbConn) 
    -> Result<Json<Product>, ApiError> {
    use crate::schema::product::dsl::*;
    let mut result = conn
        .run(move |conn| {
            product
                .filter(product_id.eq(id))
                .limit(1)
                .load::<Product>(conn)
        })
        .await?;
    Ok(Json(result.remove(0)))
}
