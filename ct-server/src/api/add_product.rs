use {
    crate::model::*,
    crate::schema::product,
    crate::session::SellerSession,
    crate::DbConn,
    diesel::prelude::*,
    rocket::form::{Form, FromForm},
    rocket::http::Status,
    rocket::serde::{json::Json, Deserialize},
};

#[derive(Debug, Deserialize, FromForm, Insertable)]
#[table_name = "product"]
pub struct CreateProduct {
    #[field(default = None)]
    seller_id: i64,
    name: String,
    description: String,
    #[field(validate = range(1..))]
    price: i32,
    #[field(validate = range(1..))]
    available_stock: i32,
}

#[post("/api/seller/add_product", data = "<product>")]
pub async fn add_product(
    session: SellerSession,
    conn: DbConn,
    mut product: Form<CreateProduct>,
) -> Status {
    product.seller_id = session.id;
    let inserted_product = conn
        .run(move |conn| {
            diesel::insert_into(product::table)
                .values(&*product)
                .get_result::<Product>(conn)
        })
        .await;
    if let Err(_) = inserted_product {
        return Status::InternalServerError;
    }
    Status::Ok
}

#[post("/api/seller/product/<product_id>/add_photo", data = "<image>")]
pub async fn add_photos(
    session: SellerSession,
    conn: DbConn,
    product_id: i64,
    image: Json<Vec<u8>>,
) {
    // TODO : implement some sort of photos adding here we might want to store in an postgres
    // of btea or some other way still in consideration
    // https://www.postgresql.org/docs/14/arrays.html#ARRAYS-DECLARATION
    unimplemented!();
}
