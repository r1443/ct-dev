use {diesel::Queryable, serde::Deserialize, serde::Serialize};

#[derive(Queryable, Debug, Clone)]
pub struct User {
    pub user_id: i64,
    pub password: String,
    pub name: String,
    pub age: i32,
    pub email: Option<String>,
    pub phone_no: Option<String>,
}

#[derive(Queryable, Debug, Clone)]
pub struct Seller {
    pub seller_id: i64,
    pub password: String,
    pub name: String,
    pub age: i32,
    pub email: String,
    pub phone_no: String,
}

#[derive(Queryable, Debug, Clone)]
pub struct Address {
    pub address_id: i64,
    pub name: String,
    pub locality: String,
    pub pincode: String,
    pub state: String,
    pub country: String,
    pub phone_no: String,
    pub seller_id: Option<i64>,
    pub user_id: Option<i64>,
}

#[derive(Queryable, Debug, Clone, Deserialize, Serialize)]
pub struct Product {
    pub product_id: i64,
    pub seller_id: i64,
    pub name: String,
    pub description: String,
    pub price: i32,
    pub available_stock: i32,
}

#[derive(Debug, Queryable, Clone)]
pub struct ProductOrder {
    pub id: i64,
    pub seller_id: i64,
    pub product_id: i64,
    pub address_id: i64,
    pub price: i32,
    pub user_id: i64,
    pub ordered_at: diesel::pg::data_types::PgTimestamp,
}
