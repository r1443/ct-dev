
use{
    crate::model::*,
    crate::schema::*,
    crate::DbConn,
    crate::password,
    rocket::form::{FromForm, Form},
    rocket::http::{CookieJar,Status, Cookie},
    diesel::prelude::*,
};

#[derive(FromForm, Debug, Clone)]
struct LoginInfo{
    username : String,
    password : String,
}


#[derive(Debug , Clone, FromForm, Insertable)]
#[table_name="seller_info"]
struct CreateInfo{
    name : String,
    age : i32,
    password : String,
    phone_no : String,
    email : String,
}

#[post("/sellers/create", data="<create_info>")]
async fn create(
    conn : DbConn, 
    mut create_info: Form<CreateInfo>,
    jar : &CookieJar<'_>
    )-> Status {
    create_info.password = password::hash_password(&create_info.password);
    let inserted_seller = conn.run( move |conn| {
        diesel::insert_into(seller_info::table)
            .values(&*create_info)
            .get_result::<Seller>(conn)
    }).await;
    match inserted_seller{
        Ok(seller) => {
            // TODO add seller id to private cookie and seller
            jar.add_private(Cookie::new("seller_id", seller.seller_id.to_string()));
            return Status::Created;
        },
        Err(_) => {
            return Status::Conflict;
        },
    };
}

#[post("/sellers/auth", data="<login_info>")]
async fn auth_seller(
    conn : DbConn,
    login_info : Form<LoginInfo>,
    jar : &CookieJar<'_>
    ) -> Status{
    let maybe_seller : Option<Seller> = match login_info.username.find('@'){
        Some(_) => fetch_user_by_email(&conn, login_info.username.clone()).await,
        None => fetch_user_by_phone(&conn, login_info.username.clone()).await,
    };
    if maybe_seller.is_none(){
        return Status::NotFound;
    }
    let seller = maybe_seller.unwrap();
    if seller.password != password::hash_password(&login_info.password){
        return Status::Unauthorized;
    }
    jar.add_private(Cookie::new("seller_id", seller.seller_id.to_string()));
    return Status::Ok;
}


async fn fetch_user_by_email(conn : &DbConn,
    seller_email : String) -> Option<Seller> {
    use crate::schema::seller_info::dsl::*;
    let seller = conn.run(move |conn|{
        seller_info.filter(email.eq(seller_email))
            .limit(1)
            .load::<Seller>(conn)
    }).await;
    match seller{
        Ok(mut v) => return Some(v.remove(0)),
        Err(_)  => return None,
    }
}

async fn fetch_user_by_phone(
    conn : &DbConn,
    seller_phone : String) -> Option<Seller>{
    use crate::schema::seller_info::dsl::*;
    let seller = conn.run(move |conn| {
        seller_info.filter(phone_no.eq(seller_phone))
            .limit(1)
            .load::<Seller>(conn)
    }).await;
    match seller{
        Ok(mut v) => return Some(v.remove(0)),
        Err(_)  => return None,
    };
}

pub fn get_routes() -> Vec<rocket::Route>{
    routes![create, auth_seller]
}

