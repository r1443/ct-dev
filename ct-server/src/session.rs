use{
    rocket::request::{FromRequest, Outcome, Request},
    rocket::http::Status,
    rocket::serde::*,
};


#[derive(Serialize, PartialEq, Eq, Deserialize, Clone, Debug)]
pub struct UserSession{
    pub id : i64
}

#[derive(Debug)]
pub enum LoginError{
    UnauthorizedError
}


#[async_trait]
impl<'r> FromRequest<'r> for UserSession{
    type Error = LoginError;
    async fn from_request(request : &'r Request<'_>) 
        -> Outcome<UserSession, Self::Error>{
        let cookies = request.cookies();
        match cookies.get_private("auth"){
            Some(maybe_session) => {
                if let Some(session) =  json::from_str(maybe_session.value()).ok(){
                    return Outcome::Success(session);
                }
                return Outcome::Failure((Status::Unauthorized, LoginError::UnauthorizedError));
            },
            None => {
                return Outcome::Failure((Status::Unauthorized, LoginError::UnauthorizedError));
            },
        };
    }
}



#[derive(Serialize, PartialEq, Eq, Deserialize, Clone, Debug)]
pub struct SellerSession{
    pub id : i64
}

#[async_trait]
impl<'r> FromRequest<'r> for SellerSession{
    type Error = LoginError;
    async fn from_request(request : &'r Request<'_>)
        -> Outcome<SellerSession, Self::Error>{
        let cookies = request.cookies();
        match cookies.get_private("auth"){
            Some(maybe_session) =>{
                if let Some(session) = json::from_str(maybe_session.value()).ok(){
                    return Outcome::Success(session);
                }
                return Outcome::Failure((Status::Unauthorized, LoginError::UnauthorizedError));
            },
            None=> {
                return Outcome::Failure((Status::Unauthorized, LoginError::UnauthorizedError));
            },
        };
    }
}
