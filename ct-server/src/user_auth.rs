use {
    crate::model::*,
    crate::password,
    crate::schema::*,
    crate::session::UserSession,
    crate::DbConn,
    diesel::prelude::*,
    diesel::Insertable,
    rocket::form::{Form, FromForm},
    rocket::http::{Cookie, CookieJar, Status},
    rocket::serde::{json::serde_json, Deserialize},
};

//type Result<T, E = Debug<diesel::result::Error>> = std::result::Result<T, E>;

#[derive(Debug, Deserialize, Clone, FromForm)]
struct CreateInfo {
    name: String,
    age: i32,
    password: String,
    phone_no: Option<String>,
    email: Option<String>,
}

#[derive(FromForm, Deserialize)]
struct LoginInfo {
    username: String,
    password: String,
}

#[derive(Insertable)]
#[table_name = "user_info"]
struct InsertUser {
    name: String,
    password: String,
    age: i32,
    email: Option<String>,
    phone_no: Option<String>,
}

#[post("/users/create", data = "<create_info>")]
async fn create(conn: DbConn, create_info: Form<CreateInfo>, jar: &CookieJar<'_>) -> Status {
    let userinfo = InsertUser {
        name: create_info.name.clone(),
        password: password::hash_password(&create_info.password),
        age: create_info.age,
        email: if let Some(email) = &create_info.email {
            Some(email.clone())
        } else {
            None
        },
        phone_no: if let Some(phone_no) = &create_info.phone_no {
            Some(phone_no.clone())
        } else {
            None
        },
    };
    //conn.run(move |conn|{ diesel::select(userinfo::table)

    //} )
    let inserted_user = conn
        .run(move |conn| {
            diesel::insert_into(user_info::table)
                .values(&userinfo)
                .get_result::<User>(conn)
        })
        .await;
    match inserted_user {
        Ok(user) => {
            // everything is ok so we are adding cookies
            let session = UserSession { id: user.user_id };
            println!("{}", serde_json::to_string(&session).unwrap());
            jar.add_private(Cookie::new(
                "auth",
                serde_json::to_string(&session).unwrap(),
            ));
            jar.add(Cookie::new("name", user.name));
            if let Some(email) = user.email {
                jar.add(Cookie::new("email", email));
            }
            if let Some(phone_no) = user.phone_no {
                jar.add(Cookie::new("phone_no", phone_no));
            }
            jar.add(Cookie::new("age", user.age.to_string()));
            return Status::Created;
        }
        Err(_) => {
            return Status::Conflict;
        }
    };
}

#[post("/users/auth", data = "<login_info>")]
async fn auth_user(conn: DbConn, login_info: Form<LoginInfo>, jar: &CookieJar<'_>) -> Status {
    let maybe_user: Option<User> = match login_info.username.find('@') {
        // this checks if username is an email or not
        Some(_) => fetch_user_by_email(&conn, login_info.username.clone()).await,
        None => fetch_user_by_phone(&conn, login_info.username.clone()).await,
    };
    if let Some(user) = maybe_user {
        let hashed_password = password::hash_password(&login_info.password);
        if hashed_password == user.password {
            let session = UserSession { id: user.user_id };
            jar.add_private(Cookie::new(
                "auth",
                serde_json::to_string(&session).unwrap(),
            ));
            jar.add(Cookie::new("name", user.name));
            if let Some(email) = user.email {
                jar.add(Cookie::new("email", email));
            }
            if let Some(phone_no) = user.phone_no {
                jar.add(Cookie::new("phone_no", phone_no));
            }
            jar.add(Cookie::new("age", user.age.to_string()));
        }
        return Status::Ok;
    }
    return Status::Unauthorized;
}

#[get("/users/logout")]
fn logout(jar: &CookieJar<'_>) {
    jar.remove_private(Cookie::named("auth"));
}

#[post("/users/delete", data = "<login_info>")]
async fn delete_user(conn: DbConn, login_info: Form<LoginInfo>) -> Status {
    use crate::schema::user_info::dsl::*;
    let maybe_user: Option<User> = match login_info.username.find('@') {
        // this checks if username is an email or not
        Some(_) => fetch_user_by_email(&conn, login_info.username.clone()).await,
        None => fetch_user_by_phone(&conn, login_info.username.clone()).await,
    };

    if let Some(user) = maybe_user {
        // using crate here because of the name clash with user_info::password
        // column
        if crate::password::hash_password(&login_info.password) == user.password {
            let _ = conn
                .run(move |conn| {
                    diesel::delete(user_info.filter(user_id.eq(user.user_id))).execute(conn)
                })
                .await;
            return Status::Ok;
        }
        return Status::Unauthorized;
    }
    return Status::NotFound;
}

async fn fetch_user_by_email(conn: &DbConn, user_email: String) -> Option<User> {
    use crate::schema::user_info::dsl::*;
    let user = conn
        .run(move |conn| {
            user_info
                .filter(email.eq(user_email))
                .limit(1)
                .load::<User>(conn)
        })
        .await;
    match user {
        Ok(mut v) => return Some(v.remove(0)),
        Err(_) => return None,
    }
}

async fn fetch_user_by_phone(conn: &DbConn, user_phone: String) -> Option<User> {
    use crate::schema::user_info::dsl::*;
    let user = conn
        .run(move |conn| {
            user_info
                .filter(phone_no.eq(user_phone))
                .limit(1)
                .load::<User>(conn)
        })
        .await;
    match user {
        Ok(mut v) => return Some(v.remove(0)),
        Err(_) => return None,
    }
}

pub fn get_routes() -> Vec<rocket::Route> {
    routes![create, auth_user, delete_user, logout]
}

#[cfg(test)]
mod tests {
    use crate::launch;
    use rocket::http::{ContentType, Status};
    use rocket::local::blocking::Client;
    #[test]
    fn check_user_creation() {
        let client = Client::tracked(launch()).unwrap();
        let req = client
            .post("/api/users/create")
            .header(ContentType::Form)
            .body(r#"name=Aman&age=18&password=archi&email=lunchspider03@gmail.com"#);
        let response = req.dispatch();
        assert_eq!(response.status(), Status::Created);
        let req = client
            .post("/api/users/delete")
            .header(ContentType::Form)
            .body(r#"username=lunchspider03@gmail.com&password=archi"#);
        let response = req.dispatch();
        assert_eq!(response.status(), Status::Ok);
        let req = client
            .post("/api/users/create")
            .header(ContentType::Form)
            .body(r#"name=Aman&age=18&password=archi&email=lunchspider03@gmail.com"#);
        let response = req.dispatch();
        let req = client.get("/api/product/1/buy");
        let response = req.dispatch();
        println!("the response from buying product is{:?}", response.status());
        let req = client
            .post("/api/users/delete")
            .header(ContentType::Form)
            .body(r#"username=lunchspider03@gmail.com&password=archi"#);
        let response = req.dispatch();
        assert_eq!(response.status(), Status::Ok);
    }
}
