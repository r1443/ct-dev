#[macro_use] extern crate rocket;
#[macro_use] extern crate diesel;
#[macro_use] extern crate diesel_full_text_search;
use rocket_sync_db_pools::database;

mod api;
mod session;
mod user_auth;
mod schema;
mod model;
mod seller_auth;
mod password;

use rocket::fs::{FileServer, relative};

#[get("/")]
async fn hello() -> &'static str{
    "you came the wrong way!!!"
}

/// database connection to postgres
#[database("test_database_ct")]
pub struct DbConn(diesel::PgConnection);

#[launch]
pub fn launch() -> _ {
    rocket::build()
        .mount("/", routes![hello])
        .mount("/api", user_auth::get_routes())
        .mount("/api", seller_auth::get_routes())
        .mount("/api", api::get_routes())
        // using file server to serve js and html files
        .mount("/", FileServer::from(relative!("./static")))
        .attach(DbConn::fairing())
}
