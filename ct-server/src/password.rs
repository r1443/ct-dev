use sha2::{Sha512, Digest};

pub fn hash_password(password : &String) -> String{
    let mut hasher = Sha512::new();
    hasher.update(password);
    let result = format!("{:X}",hasher.finalize());
    return result;
}

