table! {
    address (address_id) {
        address_id -> Int8,
        name -> Text,
        locality -> Text,
        pincode -> Text,
        state -> Text,
        country -> Text,
        phone_no -> Text,
        seller_id -> Nullable<Int8>,
        user_id -> Nullable<Int8>,
    }
}

table! {
    product (product_id) {
        product_id -> Int8,
        seller_id -> Int8,
        name -> Text,
        description -> Text,
        price -> Int4,
        available_stock -> Int4,
    }
}

table! {
    product_order (id) {
        id -> Int8,
        seller_id -> Int8,
        product_id -> Int8,
        address_id -> Int8,
        price -> Int4,
        user_id -> Int8,
        ordered_at -> Timestamptz,
    }
}

table! {
    seller_info (seller_id) {
        seller_id -> Int8,
        password -> Text,
        name -> Text,
        age -> Int4,
        email -> Text,
        phone_no -> Text,
    }
}

table! {
    user_info (user_id) {
        user_id -> Int8,
        password -> Text,
        name -> Text,
        age -> Int4,
        email -> Nullable<Text>,
        phone_no -> Nullable<Text>,
    }
}

joinable!(address -> seller_info (seller_id));
joinable!(address -> user_info (user_id));
joinable!(product -> seller_info (seller_id));
joinable!(product_order -> address (address_id));
joinable!(product_order -> product (product_id));
joinable!(product_order -> user_info (user_id));

allow_tables_to_appear_in_same_query!(
    address,
    product,
    product_order,
    seller_info,
    user_info,
);
